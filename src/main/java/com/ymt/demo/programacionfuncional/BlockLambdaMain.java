package com.ymt.demo.programacionfuncional;

public class BlockLambdaMain {


    public static void main (String args[]) {
        // Block lambda to reverse string
        MyString2 reverseStr = (str) -> {
            String result = "";

            for(int i = str.length()-1; i >= 0; i--)
                result += str.charAt(i);

            return result;
        };

        // Output: omeD adbmaL
        System.out.println(reverseStr.myStringFunction("Lambda Demo"));
    }
}
