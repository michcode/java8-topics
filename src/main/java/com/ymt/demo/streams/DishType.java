package com.ymt.demo.streams;

public enum DishType {

    FISH, OTHER, MEAT
}
