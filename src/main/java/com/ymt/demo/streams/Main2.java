package com.ymt.demo.streams;

import java.util.*;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.groupingBy;

public class Main2 {

    public static void main(String... args) {

        //List<Dish> menues = new ArrayList<>();//Cuando se obvia el objeto, "operador diamante"
        List<Dish> menues= Arrays.asList(
                new Dish("pork", false,800, DishType.MEAT),
                new Dish("beef", true,700, DishType.MEAT),
                new Dish("chicken", false,400, DishType.MEAT),
                new Dish("french fries", true,530, DishType.OTHER),
                new Dish("rice", false,350, DishType.OTHER),
                new Dish("season fruit", true,120, DishType.OTHER),
                new Dish("pizza", false,550, DishType.OTHER),
                new Dish("prawns", true,300, DishType.FISH),
                new Dish("salmon", true,450, DishType.FISH));


        //I=C
        //Clase padre....clase hija

        /**
         * Trabajando con java 7 o menos
         */
        List<Dish> lowCaloricDishes = new ArrayList<>();
        for (Dish d : menues) {
            if (d.getCalories() < 400) {
                lowCaloricDishes.add(d);
            }
        }

        /**
         * Versión con clase anónima
         */
        Collections.sort(lowCaloricDishes, comparing(Dish::getCalories));
        System.out.println();

        List<String> lowCaloricsDishesName = new ArrayList<>();
        for (Dish d : lowCaloricDishes) {
            lowCaloricsDishesName.add(d.getName());
        }
    }
}
