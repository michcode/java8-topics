package com.ymt.demo.streams;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Tener en cuenta esta parte d elos import estáticos, que permite crear código más limpio
 */
import static java.util.Comparator.comparing;

import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class ConJava8 {

    public static void main(String... args) {

        List<Dish> menues = Arrays.asList(
                new Dish("pork", false, 800, DishType.MEAT),
                new Dish("beef", true, 700, DishType.MEAT),
                new Dish("chicken", false, 400, DishType.MEAT),
                new Dish("french fries", true, 530, DishType.OTHER),
                new Dish("rice", false, 350, DishType.OTHER),
                new Dish("season fruit", true, 120, DishType.OTHER),
                new Dish("pizza", false, 550, DishType.OTHER),
                new Dish("prawns", true, 300, DishType.FISH),
                new Dish("salmon", true, 450, DishType.FISH));

        /**
         * Stream
         */
        List<String> lowCaloricsDishesName =
                menues.stream()
                        .filter(d -> d.getCalories() < 400)
                        .sorted(comparing(Dish::getCalories)) // ordena, compara.....Nombre de referencia a método
                        .map(Dish::getName) //solo quiero...
                        .collect(toList());
        //System.out.println(lowCaloricsDishesName);

        /**
         * ParallelStream
         */
        List<String> lowCaloricsDishesNames =
                menues.parallelStream()
                        .filter(d -> d.getCalories() < 400)
                        .sorted(comparing(Dish::getCalories))
                        .map(Dish::getName)
                        .collect(toList());
        //System.out.println(lowCaloricsDishesNames);

        /**
         *  Trabajando con map, agrupando comida por tipo
         */
        Map<DishType, List<Dish>> dishesByType =
                menues.stream()
                        .collect(groupingBy(Dish::getType));
        // System.out.println(dishesByType);

        /**
         * Mostrar lista de los tres con más altas calorías
         */
        List<String> threeHighCaloricDishNames =
                menues.stream()
                        .filter(d -> d.getCalories() > 300)
                        .map(Dish::getName)
                        .limit(3) //Limitar los tres primeros
                        .collect(toList());
        //System.out.println(threeHighCaloricDishNames);


        /**
         * Los streams hacen recorridos una sola vez, comprobemos
         *
         */
        List<String> title = Arrays.asList("Java", "in", "Action");

        Stream<String> s =
                title.stream();
        //s.forEach(System.out::println);
        //s.forEach(System.out::println);

        /**
         * Iteración interna con streams....ya no se piensa en loop n los for
         */
        List<String> names =
                menues.stream()
                        .map(Dish::getName)
                        .collect(toList());
        //System.out.println(names);

        /**
         * Operaciones intermedias y terminales...ejemplo de op terminales
         *
         */
        //menues.stream().forEach(System.out::println);

        long count = menues.stream()
                .filter(d -> d.getCalories() > 300)
                .distinct()
                .count();
        //System.out.println(count);


        /**
         * Filtering y slicing
         */
        List<Dish> vegetarianMenu =
                menues.stream()
                        .filter(Dish::isVegetarian)
                        .collect(toList());
        //System.out.println(vegetarianMenu);

        /**
         * Filtrando elementos únicos
         */
        List<Integer> numbers = Arrays.asList(1, 2, 3, 3, 4, 4, 5, 5, 6, 3);

        /*numbers.stream()
                .filter(i->i%2==0)
                .distinct()
                .forEach(System.out::println);*/

        /**
         * Truncando un stream
         */
        List<Dish> dishes =
                menues.stream()
                        .filter(d -> d.getCalories() > 300)
                        .limit(3)//este trunca
                        .collect(toList());
        //System.out.println(dishes);

        /**
         * Saltando elementos
         */
        List<Dish> skipDishes =
                menues.stream()
                        .filter(d -> d.getCalories() > 300)
                        .skip(4)
                        .collect(toList());
        //System.out.println(skipDishes);

        /**
         * Ejemplo filtrando los dos primero platos de carne
         */
        List<Dish> dishes1 =
                menues.stream()
                        .filter(d -> d.getType() == DishType.MEAT)
                        .limit(2)
                        .collect(toList());
        //System.out.println(dishes1);

        /*...................................Mapping......................................*/

        /**
         * Trabajando con Mapping
         */
        List<String> dishNames=menues.stream()
                .map(Dish::getName)
                .collect(toList());
        //System.out.println(dishNames);

        /**
         * Longitud de nombre d e cada plato
         */
        List<Integer> dishesNames=menues.stream()
                .map(Dish::getName)
                .map(String::length)
                .collect(toList());
        //System.out.println(dishesNames);

        /*...................................Flatening Streams......................................*/



        /*...................................Buscar coincidencias......................................*/

        /**
         * Buscando si al menos existe una coincidencia
         */
        if (menues.stream().anyMatch(Dish::isVegetarian)) {
            //System.out.println("El menu es vegetariano");
        }

        /**
         * Buscando si el predicado coincide con todos los elementos.
         */

        boolean isHealthy=menues.stream()
                .allMatch(d->d.getCalories()<1000);
        //System.out.println(isHealthy);

        /**
         * Opuesto de allMatch...noneMatch
         */

        boolean isHealthy1=menues.stream()
                .noneMatch(d->d.getCalories()>=1000);
        //System.out.println(isHealthy1);

        /**
         * Opcional (nuevo en java 8)
         */
        //Encontrar q un plato sea vegetariano.....usando filter y findAny
        /*Optional<Dish> optionalDish=menues.stream()
                .filter(Dish::isVegetarian)
                .findAny()
                .ifPresent(d->System.out.println(d.getName());*/

        /*...................................Trabajando con reduce......................................*/

        /**
         * Primera forma
         */
        int sum=numbers.stream()
                .reduce(0,(a,b)->a+b);

        /**
         * Trabajando con método por referencia
         */
        int sum1=numbers.stream()
                .reduce(0, Integer::sum);

        /**
         * Producto de elementos
         */
        int prod=numbers.stream()
                .reduce(1,(a,b)->a+b);

        /*...................................Calculando máximo y mínimo......................................*/

        /**
         * Minimo
         */
        Optional<Integer> minimo=numbers.stream()
                .reduce(Integer::min);

        /**
         * Maximo
         */
        Optional<Integer> maximo=numbers.stream()
                .reduce(Integer::max);


        /**
         * Ejercicio: como contarías el númer de platos en un stream usando map y reduce?
         */

        int counta=menues.stream()
                .map(d->1)
                .reduce(0, (a,b) -> a+b);

        //Otra manera
        long countas=menues.stream()
                .count();


        /**
         * Usando paralelismo
         */
        int contador=numbers.parallelStream()
                .reduce(0,Integer::sum);

    }
}
