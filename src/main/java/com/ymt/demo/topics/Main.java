package com.ymt.demo.topics;

import java.util.Arrays;
import static java.util.Comparator.comparing;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class Main {

    public static void main(String... args) {

        Comerciante mich = new Comerciante("Michcode", "Ate");
        Comerciante jose = new Comerciante("Jose", "Miraflores");
        Comerciante anita = new Comerciante("Anita", "Lima");
        Comerciante bad = new Comerciante("Bad", "Ventanilla");

        List<Transaccion> transaccions = Arrays.asList(
                new Transaccion(mich, 2017, 300),
                new Transaccion(jose, 2018, 1000),
                new Transaccion(jose, 2017, 400),
                new Transaccion(anita, 2018, 710),
                new Transaccion(anita, 2017, 300),
                new Transaccion(bad, 2018, 950));

        List<Transaccion> list= transaccions.stream()
                .filter(d->d.getAnio()==2017)
                .sorted(comparing(Transaccion::getValor))
                .collect(toList());
        //System.out.println(list);

        List<String> ciudades=transaccions.stream()
                .map(d->d.getComerciante().getCiudad())
                .distinct()
                .collect(toList());
        //System.out.println(list);

        List<Comerciante> ventanilla=transaccions.stream()
                .map(Transaccion::getComerciante)
                .filter(c->c.getCiudad().equals("Ventanilla"))
                .distinct()
                .sorted(comparing(Comerciante::getNombre))
                .collect(toList());
        //System.out.println(ventanilla);

        List<Comerciante> nombres=transaccions.stream()
                .map(Transaccion::getComerciante)
                .sorted(comparing(Comerciante::getNombre))
                .collect(toList());
        //System.out.println(nombres);

        boolean chorrillos=transaccions.stream()
                .anyMatch(ch->ch.getComerciante().getCiudad().equals("Ventanilla"));
        //System.out.println(chorrillos);

        /*transaccions.stream()
                .filter(t -> "Ventanilla".equals(t.getComerciante().getCiudad()))
                .map(Transaccion::getValor)
                .forEach(System.out::println);*/

        Optional<Transaccion> maximo=transaccions.stream()
                .reduce((t1,t2)->t1.getValor()>t2.getValor()?t1:t2);
        System.out.println(maximo);

        Optional<Transaccion> minimo=transaccions.stream()
                .reduce((t1,t2)->t1.getValor()<t2.getValor()?t1:t2);
        System.out.println(minimo);
    }
}
